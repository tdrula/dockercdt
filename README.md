# - Projet CdT Chocolat -

<hr>
Le prérequis pour installer le projet est d'avoir docker et docker-compose installés.
<br>

### Demarrage docker

```
git clone --recurse-submodules https://gitlab.com/tdrula/dockercdt.git afin de recupérer le projet et son submodule
Entrer la commande "./needto.sh install" pour lancer l'installation du projet
```

### Installation

Aller sur http://localhost:9003/install/index.php et cliquer sur 'Débuter l'installation'.
<br>

##### | INSTALLATION - Première partie - Etape 1

"Serveur MySQL" : <b>database</b><br>
"Nom d'utilisateur MySQL" : <b>root</b><br>
"Mot de passe MySQL " : <b>root</b><br>

"Nom de la base de données" : <b>< par-defaut ></b><br>
"Nom de votre établissement" : <b>< IPSSI-example ></b><br>

<br>

##### | INSTALLATION - Première partie - Etape 2

Cliquer sur 'Créer ma base MySql et les tables'.
<br>

##### | INSTALLATION terminé !

Si tout est correct sans messages d'erreur, alors, cliquer sur :<br>'Continuer et s'identifier en tant qu'Administrateur sans mot de passe sur la page d'accueil'.
<br>

### Accèder à la base de donnée

Dans le navigateur, renseigner : http://localhost:8083 .

<div>Ou par terminal :</div>

```
$ docker-compose exec database bash
$ mysql -uroot -p
```
### Import de la base de donnée
```
Entrer la commande "./needto.sh importBdd" pour lancer l'installation du projet
```