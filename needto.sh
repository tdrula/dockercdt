#!/bin/bash
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

install()
{
  docker-compose  up -d &&
  docker exec  dockercdt_database_1 mysql -u ${ROOT_LOGIN} --password=${ROOT_PASSWORD} -e "CREATE DATABASE cahier_de_textes"  ;
  docker exec -i dockercdt_database_1 mysql -u${ROOT_LOGIN} -p${ROOT_PASSWORD} --database=cahier_de_textes < projet-cdt-chocolat/install/installation_tables.sql;

}

reset()
{
  docker-compose exec  database mysql -u ${ROOT_LOGIN} --password=${ROOT_PASSWORD} -e "DROP DATABASE IF EXISTS cahier_de_textes" &&
  docker exec  dockercdt_database_1 mysql -u ${ROOT_LOGIN} --password=${ROOT_PASSWORD} -e "CREATE DATABASE cahier_de_textes" &&
  docker exec -i dockercdt_database_1 mysql -u${ROOT_LOGIN} -p${ROOT_PASSWORD} --database=cahier_de_textes < projet-cdt-chocolat/install/installation_tables.sql;
}

drop()
{
    docker-compose exec  database mysql -u ${ROOT_LOGIN} --password=${ROOT_PASSWORD} -e "DROP DATABASE IF EXISTS cahier_de_textes" ;
}

importBdd()
{
  docker-compose exec -i database mysqldump -u root --password='root' cahier_de_textes > backup.sql &&
  echo "backup created";
}

restart()
{
  docker-compose restart;
}

destroy()
{
  docker-compose stop;
}


printHelp()
{
printf $"Usage: $0
            install\t\t\tdestroy and install everything
{then the container}(optional)
"
}

secondOption()
{
   subcommand=''

     while [ -n "$1" ]; do
        case "$1" in
            install\
            |restart \
            |reset \
            |drop \
            |importBdd \
            |destroy)
                subcommand=$1
            ;;
             --with-pgadmin)
                composeFile="${composeFile} -f docker-compose-pgadmin.yml"
            ;;
            *)
                argument=$1
            ;;
       esac
       shift
    done
    if [ -n "$(LC_ALL=C type -t $subcommand)" ]  && [ "$(LC_ALL=C type -t $subcommand)" = function ]; then
      $subcommand
    else
      printHelp
    fi
}

secondOption "$1" "$2"
